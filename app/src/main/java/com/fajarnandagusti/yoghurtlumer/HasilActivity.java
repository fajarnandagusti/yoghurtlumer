package com.fajarnandagusti.yoghurtlumer;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class HasilActivity extends AppCompatActivity {


    TextView tvNama, tvRasa, tvTglProduksi, tvTglExpire, tvKomposisi;
    String namaProduk, rasaProduk, tglProduksi, tglExpire, komposisi;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hasil);

        //Binding ID / Variabel
        tvNama = findViewById(R.id.namaProduk);
        tvRasa = findViewById(R.id.rasaProudk);
        tvTglProduksi = findViewById(R.id.tglProduksi);
        tvTglExpire = findViewById(R.id.tglExpire);
        tvKomposisi = findViewById(R.id.komposisi);


        //GetIntentExtra untuk mengambil data yg dikirim dari intent extra Main Activity
        namaProduk = getIntent().getStringExtra("namaProduk");
        rasaProduk = getIntent().getStringExtra("rasaProduk");
        komposisi = getIntent().getStringExtra("komposisi");
        tglProduksi = getIntent().getStringExtra("tglProduksi");
        tglExpire = getIntent().getStringExtra("tglExpire");

        //Binding value data ke text view
        tvNama.setText(namaProduk);
        tvRasa.setText(rasaProduk);
        tvTglProduksi.setText(tglProduksi);
        tvTglExpire.setText(tglExpire);
        tvKomposisi.setText(komposisi);


    }
}
