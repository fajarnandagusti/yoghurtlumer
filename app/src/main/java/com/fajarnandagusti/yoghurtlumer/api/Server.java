package com.fajarnandagusti.yoghurtlumer.api;

import static com.fajarnandagusti.yoghurtlumer.utility.Utility.BASE_URL_API;

public class Server {
    public static ApiService getAPIService() {
        return Client.getClient(BASE_URL_API).create(ApiService.class);

    }
}
