package com.fajarnandagusti.yoghurtlumer.api;

import com.fajarnandagusti.yoghurtlumer.model.ResponseData;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiService {
    @FormUrlEncoded
    @POST("cekKedaluwarsa.php")
    Call<ResponseData> cekExpire(@Field("kode")String qrcode);
}
