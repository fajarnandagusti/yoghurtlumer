package com.fajarnandagusti.yoghurtlumer;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Toast;

import com.fajarnandagusti.yoghurtlumer.api.ApiService;
import com.fajarnandagusti.yoghurtlumer.api.Server;
import com.fajarnandagusti.yoghurtlumer.model.ResponseData;
import com.fajarnandagusti.yoghurtlumer.pref.PrefManager;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    CardView btnScan, btnAbout, btnPetunjuk, btnTentang;

    ProgressDialog pDialog;
    IntentIntegrator intentIntegrator;

    ApiService API;

    String namaProduk, rasaProduk, tglProduksi, tglExpire, komposisi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnScan = findViewById(R.id.btnScan);
        btnAbout = findViewById(R.id.btnAbout);
        btnPetunjuk = findViewById(R.id.btnPetunjuk);
        btnTentang = findViewById(R.id.btnTentang);

        API = Server.getAPIService();
        
        
        btnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                
                intentIntegrator = new IntentIntegrator(MainActivity.this);
                intentIntegrator.initiateScan();
            }
        });

        btnTentang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, TentangActivity.class);
                startActivity(intent);
            }
        });

        btnAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AboutActivity.class);
                startActivity(intent);
            }
        });

        btnPetunjuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PrefManager prefManager = new PrefManager(getApplicationContext());
                prefManager.setFirstTimeLaunch(true);
                startActivity(new Intent(MainActivity.this, PetunjukActivity.class));
                finish();
            }
        });


    }
    
    
    protected void onActivityResult(int requestCode, int resultCoode, Intent data){
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCoode, data);
        
        if (result != null){
            
            if (result.getContents() == null){
                // Toast.makeText(this, "TIDAK ADA DATA", Toast.LENGTH_SHORT).show();
            }else {
                
                try{
                

                String kode = result.getContents();
                
                Cek(kode);
                
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            
        }else {
            super.onActivityResult(requestCode, resultCoode, data);
        }
    }

    private void Cek(String kode) {
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(true);
        pDialog.setMessage("Tunggu");
        pDialog.show();

        API.cekExpire(kode).enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {

                try{
                    if (response.isSuccessful()){
                        ResponseData responseData = response.body();

                        assert responseData != null;

                        if (responseData.getSuccess().equals("1")){
                            pDialog.cancel();
                            Toast.makeText(MainActivity.this, responseData.getTgl_expire(), Toast.LENGTH_LONG).show();

                            namaProduk = responseData.getNama();
                            rasaProduk = responseData.getRasa();
                            komposisi = responseData.getKomposisi();
                            tglProduksi = responseData.getTgl_produksi();
                            tglExpire = responseData.getTgl_expire();

                            //IntentExtra untuk membawa data ke halaman hasil scan
                            Intent i = new Intent (MainActivity.this, HasilActivity.class);
                            i.putExtra("namaProduk", namaProduk);
                            i.putExtra("rasaProduk", rasaProduk);
                            i.putExtra("komposisi", komposisi);
                            i.putExtra("tglProduksi", tglProduksi);
                            i.putExtra("tglExpire", tglExpire);

                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);


                        }else {
                            pDialog.cancel();
                            Toast.makeText(MainActivity.this, responseData.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }else {
                        ResponseData responseData = response.body();
                        pDialog.cancel();
//                        Toast.makeText(MainActivity.this, "Success 0"+responseData.getMessage(), Toast.LENGTH_LONG).show();
                        Toast.makeText(MainActivity.this, responseData.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseData> call, Throwable t) {
                pDialog.cancel();
                Toast.makeText(MainActivity.this, "FAILURE"+t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });



    }


}
