package com.fajarnandagusti.yoghurtlumer.model;

import com.google.gson.annotations.SerializedName;

public class ResponseData {

    @SerializedName("success")
    String success;

    @SerializedName("nama")
    String nama;

    @SerializedName("rasa")
    String rasa;

    @SerializedName("tgl_produksi")
    String tgl_produksi;

    @SerializedName("tgl_expire")
    String tgl_expire;

    @SerializedName("message")
    String message;

    @SerializedName("status")
    String status;

    @SerializedName("komposisi")
    String komposisi;



    public String getKomposisi() {
        return komposisi;
    }

    public void setKomposisi(String komposisi) {
        this.komposisi = komposisi;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getRasa() {
        return rasa;
    }

    public void setRasa(String rasa) {
        this.rasa = rasa;
    }

    public String getTgl_produksi() {
        return tgl_produksi;
    }

    public void setTgl_produksi(String tgl_produksi) {
        this.tgl_produksi = tgl_produksi;
    }

    public String getTgl_expire() {
        return tgl_expire;
    }

    public void setTgl_expire(String tgl_expire) {
        this.tgl_expire = tgl_expire;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }


    public ResponseData(String success, String nama, String rasa, String tgl_produksi, String tgl_expire, String message, String status, String komposisi) {
        this.success = success;
        this.nama = nama;
        this.rasa = rasa;
        this.tgl_produksi = tgl_produksi;
        this.tgl_expire = tgl_expire;
        this.message = message;
        this.status = status;
        this.komposisi = komposisi;
    }
}
